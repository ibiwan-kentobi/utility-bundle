<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 11:11 AM
 */

namespace Jtangas\UtilityBundle\DataModel;


use Jtangas\UtilityBundle\Interfaces\ModelableInterface;

interface DataModelInterface
{
    public function format(ModelableInterface $data = null, $format = null, $options = []);
}