<?php
/**
 * Created by PhpStorm.
 * User: jtangas
 * Date: 12/20/17
 * Time: 11:13 AM
 */

namespace Jtangas\UtilityBundle\Interfaces;


interface ModelableInterface
{
    function getFormatterModel();
}